/*
 * @Description: helper
 * @Author: yao yu qing
 * @Date: 2021-03-11 14:43:40
 * @LastEditTime: 2021-04-07 16:07:50
 * @LastEditors: yyq
 * @FilePath: \Block-la\src\helpers\helpers.js
 */
let runtime = require('art-template/lib/runtime');

// 格式化时间戳
runtime.dateFormat = function dateFormat(date, format) {
  date = date === 0 ? new Date() : new Date(date);
  var map = {
    M: date.getMonth() + 1, // 月份
    d: date.getDate(), // 日
    h: date.getHours(), // 小时
    m: date.getMinutes(), // 分
    s: date.getSeconds(), // 秒
    q: Math.floor((date.getMonth() + 3) / 3), // 季度
    S: date.getMilliseconds() // 毫秒
  };
  format = format.replace(/([yMdhmsqS])+/g, function (all, t) {
    var v = map[t];
    if (v !== undefined) {
      if (all.length > 1) {
        v = '0' + v;
        v = v.substr(v.length - 2);
      }
      return v;
    } else if (t === 'y') {
      return (date.getFullYear() + '').substr(4 - all.length);
    }
    return all;
  });
  return format;
};

// 个位补零
runtime.zero = function zero(num, n) {
  if ((num + '').length >= n) return num;
  return zero('0' + num, n);
};

module.exports = runtime;
