/* eslint-disable */
export default function renderWave(e, i, t) {
  let playing = false;
  if (((playing = !!t), e.length > 0)) {
    if (!t) {
      var a = new IntersectionObserver(
        function (e, i) {
          e.forEach(function (e) {
            playing = !!e.isIntersecting;
          });
        },
        { rootMargin: '0px 0px -250px 0px' }
      );

      i.forEach(function (e) {
        a.observe(e);
      });
    }
    var n;
    var r;
    var o;
    var s;
    var l = 100;
    var d = 200;
    var c = 0;
    window.innerWidth, window.innerHeight;
    function u() {
      window.innerWidth / 2,
        window.innerHeight / 2,
        n.updateProjectionMatrix(),
        o.setSize(
          window.innerWidth,
          document.querySelector('.ribbon').offsetHeight
        );
    }
    !(function () {
      e,
        ((n = new THREE.PerspectiveCamera(
          45,
          window.innerWidth / 2 / (window.innerHeight / 6),
          1,
          1e4
        )).position.x = 0),
        (n.position.y = -20),
        (n.position.z = 680),
        ((r = new THREE.Scene()).rotation.x = 0.1),
        (r.rotation.y = 1.6),
        (r.rotation.z = -0.05);
      for (
        var i = 2e4 / 7,
          t = new Float32Array(6 * i),
          a = new Float32Array(i),
          c = 0,
          f = 0,
          p = 0;
        p < l;
        p++
      )
        for (var m = 0; m < d; m++)
          (t[c] = 11 * p - 550),
            (t[c + 1] = 0),
            (t[c + 2] = 11 * m - 2200 / 6),
            (a[f] = 1),
            (c += 3),
            f++;
      var g = new THREE.BufferGeometry();
      g.setAttribute('position', new THREE.BufferAttribute(t, 3)),
        g.setAttribute('scale', new THREE.BufferAttribute(a, 1));
      var v = new THREE.ShaderMaterial({
        uniforms: { color: { value: new THREE.Color(0x27669d) } },
        vertexShader: [
          'attribute float scale;',
          'void main() {',
          'vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );',
          'gl_PointSize = 1.45;',
          'gl_Position = projectionMatrix * mvPosition;',
          '}'
        ].join('\n'),
        fragmentShader: [
          'uniform vec3 color;',
          'void main() {',
          'if ( length( gl_PointCoord - vec2( 0.5, 0.5 ) ) > 0.475 ) discard;',
          'gl_FragColor = vec4( color, 1.0 );',
          '}'
        ].join('\n')
      });

      (s = new THREE.Points(g, v)),
        r.add(s),
        (o = new THREE.WebGLRenderer({ alpha: !0 })).setSize(
          window.innerWidth,
          document.querySelector('.ribbon').offsetHeight
        ),
        (r.background = null),
        e.append(o.domElement),
        o.setPixelRatio(1 * window.devicePixelRatio),
        window.addEventListener('resize', u, !1);
    })(),
      (function e() {
        requestAnimationFrame(e),
          playing &&
            (function () {
              for (
                var e = s.geometry.attributes.position.array,
                  i = s.geometry.attributes.scale.array,
                  t = 0,
                  a = 0,
                  u = 0;
                u < l;
                u++
              )
                for (var f = 0; f < d; f++)
                  (e[t + 1] =
                    12 * Math.sin(0.2 * (u + c)) +
                    24 * Math.sin(0.1 * (f + c))),
                    (i[a] =
                      2 * (Math.sin(0.1 * (u + c)) + 1) +
                      2 * (Math.sin(0.3 * (f + c)) + 1)),
                    (t += 3),
                    a++;
              (s.geometry.attributes.position.needsUpdate = !0),
                (s.geometry.attributes.scale.needsUpdate = !0),
                o.render(r, n),
                o.setClearColor(0, 0),
                (c += 0.05);
            })();
      })();
  }
}

/* eslint-enable */
