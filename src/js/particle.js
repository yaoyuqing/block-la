/* eslint-disable */
const BASE_URL = process.env.NODE_ENV === 'development' ? '/' : '';

var RenderContext = function (canvas) {
  // PRIVATE VARS

  var _this = this;

  var _canvas = canvas;
  var _renderer;
  var _w, _h, _aspect;

  var _camera;
  var _scene;
  var _controls;

  var _updateFuncs = [];

  var _imgData = {
    pending: false,
    callback: undefined,
    dataUrl: undefined
  };

  // HARDCODE PARAMS

  var _rendererParams = {
    canvas: _canvas,
    alpha: true,
    depth: true,
    stencil: false,
    antialias: false,
    premultipliedAlpha: true,
    preserveDrawingBuffer: false,
    logarithmicDepthBuffer: false,
    autoClear: false,
    clearColor: 0x0,
    clearAlpha: 0,
    sortObjects: true,
    shadowMapEnabled: false,
    shadowMapType: THREE.PCFShadowMap,
    shadowMapCullFace: THREE.CullFaceFront,
    shadowMapDebug: false
  };

  var _cameraParams = {
    fov: 45,
    near: 1,
    far: 2000
  };

  // PRIVATE FUNCTIONS

  var _initRenderer = function () {
    _renderer = new THREE.WebGLRenderer(_rendererParams);
    _renderer.setSize(_w, _h);
    _renderer.setClearColor(
      _rendererParams.clearColor,
      _rendererParams.clearAlpha
    );
    _renderer.autoClear = _rendererParams.autoClear;
    _renderer.sortObjects = _rendererParams.sortObjects;
    _renderer.shadowMap.enabled = _rendererParams.shadowMapEnabled;
    _renderer.shadowMap.type = _rendererParams.shadowMapType;
    _renderer.shadowMap.shadowSide = _rendererParams.shadowMapCullFace;
    _renderer.shadowMapDebug = _rendererParams.shadowMapDebug;
  };

  // PUBLIC FUNCTIONS

  this.init = function () {
    _w = _canvas.clientWidth;
    _h = _canvas.clientHeight;
    _aspect = _w / _h;

    _initRenderer();

    _camera = new THREE.PerspectiveCamera(
      _cameraParams.fov,
      _aspect,
      _cameraParams.near,
      _cameraParams.far
    );

    _scene = new THREE.Scene();

    // _controls = new THREE.TrackballControls(_camera, _canvas);
    // _controls.rotateSpeed = 1.0;
    // _controls.zoomSpeed = 1.2;
    // _controls.panSpeed = 1.0;
    // _controls.dynamicDampingFactor = 0.3;
    // _controls.staticMoving = false;
    // _controls.noZoom = false;
    // _controls.noPan = false;
    // _updateFuncs.push(_controls.update);

    this.customInit();
  };

  this.setSize = function (w, h) {
    _w = w;
    _h = h;
    _aspect = _w / _h;

    _renderer.setSize(_w, _h);

    _camera.aspect = _aspect;
    _camera.updateProjectionMatrix();

    // _controls.handleResize();
  };

  this.update = function (dt) {
    // _renderer.clearTarget(null);
    _renderer.setRenderTarget(null);
    _renderer.clear(null);

    for (var i = 0; i < _updateFuncs.length; i++) _updateFuncs[i](dt);

    _renderer.render(_scene, _camera);

    if (_imgData.pending) {
      _imgData.dataUrl = _renderer.domElement.toDataURL();
      _imgData.pending = false;
      if (_imgData.callback) _imgData.callback(_imgData.dataUrl);
    }
  };

  this.customInit = function () {
    _camera.position.z = 10;

    // var geometry = new THREE.BoxGeometry( 1, 1, 1 );
    // var material = new THREE.MeshBasicMaterial( { color: 0xff0000, wireframe: true } );
    // var mesh = new THREE.Mesh( geometry, material );
    // _scene.add(mesh);
  };

  this.getRenderer = function () {
    return _renderer;
  };

  this.getScene = function () {
    return _scene;
  };

  this.getCamera = function () {
    return _camera;
  };

  this.getImageData = function (cb) {
    _imgData.pending = true;
    _imgData.callback = cb;
  };
};

RenderContext.prototype.constructor = RenderContext;

var ParticleEngine = function (params) {
  var _this = this;

  var _canvas, _size;
  var _updateLoop;
  var _renderer, _camera, _scene;
  var _sim, _simMat, _initMat, _drawMat;
  var _mouse;
  var _controls, _raycaster;
  var _leapMan;
  var _customUpdate;
  var _pauseSim = false;

  // PARAMS

  params = params || {};
  _size = params.size || 512;
  _simMat = params.simMat || createShaderMaterial(BasicSimShader);
  _initMat = params.initMat || createShaderMaterial(SimInitShader);
  _drawMat = params.drawMat || createShaderMaterial(BasicParticleShader);
  _customUpdate = params.update;

  // EVENTS

  var _onWindowResize = function () {
    _renderer.setSize(window.innerWidth, window.innerHeight);
  };

  var _onFrameUpdate = function (dt, t) {
    // _stats.begin();

    _leapUpdate();

    _inputUpdate();

    if (!_controls.enabled) _controls.update();

    if (_customUpdate) _customUpdate(dt, t);

    _renderer.update(dt);
    _leapMan.render();

    // _stats.end();
  };

  var _onFixedUpdate = function (dt, t) {
    if (!_pauseSim) _sim.update(dt, t);
  };

  // PRIVATE FUNCTIONS

  var _init = function () {
    window.addEventListener('resize', _onWindowResize, false);

    // _stats = new Stats();
    // document.body.appendChild(_stats.domElement);

    _updateLoop = new UpdateLoop();
    _updateLoop.frameCallback = _onFrameUpdate;
    _updateLoop.fixedCallback = _onFixedUpdate;

    _canvas = document.querySelector('#webgl-canvas');

    _mouse = new Mouse(document.querySelector('#product'));

    _renderer = new RenderContext(_canvas);
    _renderer.init();
    _camera = _renderer.getCamera();
    _scene = _renderer.getScene();
  };

  var _sceneInit = function () {
    _sim = new ParticleSimulation(_renderer.getRenderer(), _size, {
      simMat: _simMat,
      initMat: _initMat,
      drawMat: _drawMat
    });
    _scene.add(_sim.getParticleObject());

    _camera.position.set(0, 0, 8);
    _controls = new THREE.OrbitControls(_camera, _canvas);
    _controls.rotateUp(Math.PI / 6);
    _controls.autoRotate = true;
    _controls.autoRotateSpeed = 2.0;
    _controls.noPan = true;
    _controls.enabled = false; // disable user input

    _raycaster = new THREE.Raycaster();

    var tmat = new THREE.Matrix4().compose(
      new THREE.Vector3(0.0, -3.0, -_camera.position.z),
      new THREE.Quaternion(),
      new THREE.Vector3(0.015, 0.015, 0.015)
    );
    _leapMan = new LeapManager(_renderer.getRenderer(), _camera, tmat);
    _simMat.defines.MULTIPLE_INPUT = ''; // TODO_NOP: at least even hardcode numbers for this in shader
    _simMat.needsUpdate = true;

    // _debugBox = document.querySelector('#debug-box');
  };

  var _mouseUpdate = function () {
    var mIdMax = Utils.isMobile ? 4 : 1;
    for (var mId = 0; mId < mIdMax; mId++) {
      var ms = _mouse.getMouse(mId);
      if (ms.buttons[0] || (mId === 0 && ms.buttons[2])) {
        _raycaster.setFromCamera(ms.coords, _camera);

        // from target point to camera
        var pos = _controls.target;
        var nor = pos.clone().sub(_camera.position).normalize();
        var plane = new THREE.Plane(
          nor,
          -nor.x * pos.x - nor.y * pos.y - nor.z * pos.z
        );

        // intersect plane
        var intersects = new THREE.Vector3();
        var point = _raycaster.ray.intersectPlane(plane, intersects);

        _simMat.uniforms.uInputPos.value[mId].copy(point);
        _simMat.uniforms.uInputPosAccel.value.setComponent(
          mId,
          ms.buttons[0] ? 1.0 : -1.0
        );
      } else {
        _simMat.uniforms.uInputPosAccel.value.setComponent(mId, 0);
      }
    }

    // _debugBox.innerHTML +=
    //     "<br>"+_simMat.uniforms.uInputPosAccel.value.x.toFixed(2)
    //     +" "+_simMat.uniforms.uInputPosAccel.value.y.toFixed(2)
    //     +" "+_simMat.uniforms.uInputPosAccel.value.z.toFixed(2)
    //     +" "+_simMat.uniforms.uInputPosAccel.value.w.toFixed(2);
  };

  var _leapUpdate = function () {
    var K_PULL = 1.0; // in grabStrength
    var K_PUSH = 100.0; // in sphereRadius

    _leapMan.update();

    for (var i = 0; i < _leapMan.activeHandCount; i++) {
      var inputIdx = 3 - i; // iterate backwards on input, so mouse can interact at same time
      if (_leapMan.frame.hands[i].grabStrength === K_PULL) {
        _simMat.uniforms.uInputPos.value[inputIdx].copy(
          _leapMan.palmPositions[i]
        );
        _simMat.uniforms.uInputPosAccel.value.setComponent(inputIdx, 1.0);
      } else if (_leapMan.frame.hands[i].sphereRadius >= K_PUSH) {
        _simMat.uniforms.uInputPos.value[inputIdx].copy(
          _leapMan.palmPositions[i]
        );
        _simMat.uniforms.uInputPosAccel.value.setComponent(inputIdx, -1.0);
      }
    }

    // _debugBox.innerHTML =
    //     "hand1: " + (_leapMan.frame.hands[0] ? _leapMan.frame.hands[0].sphereRadius : "") + " " +
    //     "hand2: " + (_leapMan.frame.hands[1] ? _leapMan.frame.hands[1].sphereRadius : "") +
    //     "";
  };

  var _inputUpdate = function () {
    // reset input accels
    _simMat.uniforms.uInputPosAccel.value.set(0, 0, 0, 0);
    if (!_controls.enabled) _mouseUpdate();
    _leapUpdate();
  };

  // PUBLIC FUNCTIONS

  this.start = function () {
    _updateLoop.start();
  };

  this.stop = function () {
    _updateLoop.stop();
  };

  this.pauseSimulation = function (value) {
    _pauseSim = value;
  };

  this.enableCameraAutoRotate = function (value) {
    _controls.autoRotate = value;
  };

  this.enableCameraControl = function (value) {
    _controls.enabled = value;
  };

  // INIT

  _init();

  _sceneInit();

  // expose variables
  this.renderer = _renderer;
  this.scene = _scene;
  this.camera = _camera;
};

// TODO: timer sync issue between fixed and frame (?)

var UpdateLoop = function () {
  var _this = this;

  var _timer = 0.0;
  var _timeScale = 1.0;
  var _fixedTimer = 0.0;
  var _fixedTimeRemainder = 0.0;
  var _FIXED_TIME_STEP = 0.02;
  var _FIXED_TIME_STEP_MAX = 0.2;

  var _clock = new Clock(false);

  var _requestId;
  // PRIVATE FUNCTIONS

  var _fixedUpdate = function () {
    var fixedDt = _FIXED_TIME_STEP * _timeScale;

    _fixedTimer += fixedDt;

    _this.fixedCallback(fixedDt, _fixedTimer);
  };

  var _frameUpdate = function () {
    var frameDt = _clock.getDelta();

    _timer += frameDt * _timeScale;
    _fixedTimeRemainder += frameDt;

    // cap remainder
    if (_fixedTimeRemainder > _FIXED_TIME_STEP_MAX) {
      _fixedTimeRemainder = _FIXED_TIME_STEP_MAX;
    }

    // loop remainder
    while (_fixedTimeRemainder > _FIXED_TIME_STEP) {
      _fixedUpdate();
      _fixedTimeRemainder -= _FIXED_TIME_STEP;
    }

    _this.frameCallback(frameDt, _timer);
  };

  var _loop = function () {
    _frameUpdate();
    _requestId = window.requestAnimationFrame(_loop);
  };

  // PUBLIC FUNCTIONS

  this.start = function () {
    if (!_requestId) {
      _clock.start();
      _loop();
    }
  };

  this.stop = function () {
    if (_requestId) {
      window.cancelAnimationFrame(_requestId);
      _requestId = undefined;
      _clock.stop();
    }
  };

  this.setTimeScale = function (scale) {
    _timeScale = Math.max(scale, 0);
  };

  // EMPTY FUNCTIONS TO SET

  this.fixedCallback = function (dt, t) {};
  this.frameCallback = function (dt, t) {};
};

UpdateLoop.prototype.constructor = UpdateLoop;

var Utils = {};

// pre-computed on load
Utils.isMobile = (function (a) {
  return (
    /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(
      a
    ) ||
    /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
      a.substr(0, 4)
    )
  );
})(navigator.userAgent || navigator.vendor || window.opera);

Utils.toRadians = function (degrees) {
  return (degrees * Math.PI) / 180.0;
};

Utils.toDegrees = function (rads) {
  return (rads * 180.0) / Math.PI;
};

Utils.hexToRgb = function (hex) {
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result
    ? [
        parseInt(result[1], 16),
        parseInt(result[2], 16),
        parseInt(result[3], 16)
      ]
    : null;
};

Utils.rgbToHex = function () {
  return '#' + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
};

Utils.loadTextFile = function (url) {
  var isIndex = document.querySelector('.bodyindex');
  if (!isIndex) return false;
  var result;

  var req = new XMLHttpRequest();
  req.onerror = function () {
    console.log('Error: request error on ' + url);
  };
  req.onload = function () {
    result = this.responseText;
  };
  req.open('GET', url, false);
  req.send();

  return result;
};

Utils.loadTextFileInject = function (url) {
  var isIndex = document.querySelector('.bodyindex');
  if (!isIndex) return false;
  var regex = /#inject .+/g;
  var fileStr = Utils.loadTextFile(url);
  var matches = fileStr.match(regex);

  if (!matches) return fileStr;

  for (var i = 0; i < matches.length; i++) {
    var injectLine = matches[i];
    var injectUrl = injectLine.split(' ')[1];
    var injectFileStr = Utils.loadTextFileInject(injectUrl);
    fileStr = fileStr.replace(injectLine, injectFileStr);
  }

  return fileStr;
};

Utils.openUrlInNewWindow = function (url, width, height) {
  window.open(
    url,
    'Screenshot',
    'width=' + width + ' height=' + height + ' scrollbars=no, resizable=yes'
  );
};

// https://github.com/ebidel/filer.js/blob/master/src/filer.js
Utils.dataUrlToBlob = function (dataUrl) {
  var BASE64_MARKER = ';base64,';
  if (dataUrl.indexOf(BASE64_MARKER) == -1) {
    var parts = dataUrl.split(',');
    var contentType = parts[0].split(':')[1];
    var raw = decodeURIComponent(parts[1]);

    return new Blob([raw], { type: contentType });
  }

  var parts = dataUrl.split(BASE64_MARKER);
  var contentType = parts[0].split(':')[1];
  var raw = window.atob(parts[1]);
  var rawLength = raw.length;

  var uInt8Array = new Uint8Array(rawLength);

  for (var i = 0; i < rawLength; ++i) {
    uInt8Array[i] = raw.charCodeAt(i);
  }

  return new Blob([uInt8Array], { type: contentType });
};

Utils.dataUrlToBlobUrl = function (dataUrl) {
  return window.URL.createObjectURL(Utils.dataUrlToBlob(dataUrl));
};

// https://developer.mozilla.org/en-US/docs/Web/Guide/API/DOM/Using_full_screen_mode
Utils.toggleFullscreen = function () {
  if (
    !document.fullscreenElement && // alternative standard method
    !document.mozFullScreenElement &&
    !document.webkitFullscreenElement &&
    !document.msFullscreenElement
  ) {
    // current working methods
    if (document.documentElement.requestFullscreen) {
      document.documentElement.requestFullscreen();
    } else if (document.documentElement.msRequestFullscreen) {
      document.documentElement.msRequestFullscreen();
    } else if (document.documentElement.mozRequestFullScreen) {
      document.documentElement.mozRequestFullScreen();
    } else if (document.documentElement.webkitRequestFullscreen) {
      document.documentElement.webkitRequestFullscreen(
        Element.ALLOW_KEYBOARD_INPUT
      );
    }
  } else {
    if (document.exitFullscreen) {
      document.exitFullscreen();
    } else if (document.msExitFullscreen) {
      document.msExitFullscreen();
    } else if (document.mozCancelFullScreen) {
      document.mozCancelFullScreen();
    } else if (document.webkitExitFullscreen) {
      document.webkitExitFullscreen();
    }
  }
};

// http://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript
Utils.getParameterByName = function (name) {
  name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
  var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
  var results = regex.exec(location.search);
  return results === null
    ? ''
    : decodeURIComponent(results[1].replace(/\+/g, ' '));
};

// http://bost.ocks.org/mike/shuffle/
Utils.shuffle = function (array) {
  var currentIndex = array.length;
  var temporaryValue;
  var randomIndex;

  // While there remain elements to shuffle...
  while (currentIndex !== 0) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
};

var Mouse = function (dom) {
  var _this = this;

  this.buttons = [];
  this.x = 0;
  this.y = 0;
  this.dx = 0;
  this.dy = 0;
  this.coords = new THREE.Vector2();

  var _mouseStructs = [];

  var MouseStruct = function MouseStruct() {
    this.x = 0;
    this.y = 0;
    this.dx = 0;
    this.dy = 0;
    this.coords = new THREE.Vector2();
    this.buttons = [];
  };

  // MOUSE

  var _mouseUpdate = function (e, idx) {
    var ms = _this.getMouse(idx || 0);

    ms.dx = e.pageX - ms.x;
    ms.dy = e.pageY - ms.y;
    ms.x = e.pageX;
    ms.y = e.pageY;

    ms.coords.x = (ms.x / dom.clientWidth) * 2 - 1;
    ms.coords.y = (ms.y / dom.clientHeight) * -2 + 1;
  };

  var _onMouseMove = function (e) {
    _mouseUpdate(e);
    _this.getMouse().buttons[e.button] = true;
    // e.preventDefault();
  };

  var _onMouseDown = function (e) {
    _mouseUpdate(e);
    // _this.getMouse().buttons[e.button] = true;
    // e.preventDefault();
  };

  var _onMouseUp = function (e) {
    _mouseUpdate(e);
    _this.getMouse().buttons[e.button] = false;
    // e.preventDefault();
  };

  // TOUCH

  var _onTouchMove = function (e) {
    var touches = e.changedTouches;
    for (var i = 0; i < touches.length; i++) {
      _mouseUpdate(touches[i], touches[i].identifier);
    }
    e.preventDefault();
  };

  var _onTouchDown = function (e) {
    var touches = e.changedTouches;
    for (var i = 0; i < touches.length; i++) {
      _mouseUpdate(touches[i], touches[i].identifier);
      _this.getMouse(touches[i].identifier).buttons[0] = true;
    }
    e.preventDefault();
  };

  var _onTouchUp = function (e) {
    var touches = e.changedTouches;
    for (var i = 0; i < touches.length; i++) {
      _mouseUpdate(touches[i], touches[i].identifier);
      _this.getMouse(touches[i].identifier).buttons[0] = false;
    }
    e.preventDefault();
  };

  // PUBLIC FUNCTIONS

  this.getLength = function () {
    return _mouseStructs.length;
  };

  this.getMouse = function (idx) {
    idx = idx || 0;
    if (idx >= _mouseStructs.length) _mouseStructs[idx] = new MouseStruct();

    return _mouseStructs[idx];
  };

  // BIND EVENTS

  dom.addEventListener('mousemove', _onMouseMove, false);
  dom.addEventListener('mousedown', _onMouseDown, false);
  dom.addEventListener('mouseup', _onMouseUp, false);

  dom.addEventListener('touchmove', _onTouchMove, false);
  dom.addEventListener('touchstart', _onTouchDown, false);
  dom.addEventListener('touchend', _onTouchUp, false);
  dom.addEventListener('touchleave', _onTouchUp, false);
  dom.addEventListener('touchcancel', _onTouchUp, false);
};

var cloneDefines = function (src) {
  var dst = {};
  for (var d in src) {
    dst[d] = src[d];
  }
  return dst;
};

var createShaderMaterial = function (shader) {
  return new THREE.ShaderMaterial({
    defines: cloneDefines(shader.defines),
    uniforms: THREE.UniformsUtils.clone(shader.uniforms),
    vertexShader: shader.vertexShader,
    fragmentShader: shader.fragmentShader
  });
};

// can pass in shader or material

var ShaderPass = function (shader) {
  if (shader instanceof THREE.Material) this.material = shader;
  else this.material = createShaderMaterial(shader);

  this.material.blending = THREE.NoBlending;
  this.material.depthWrite = false;
  this.material.depthTest = false;

  // http://michaldrobot.com/2014/04/01/gcn-execution-patterns-in-full-screen-passes/
  var triangle = new THREE.BufferGeometry();
  var p = new Float32Array(9);
  p[0] = -1;
  p[1] = -1;
  p[2] = 0;
  p[3] = 3;
  p[4] = -1;
  p[5] = 0;
  p[6] = -1;
  p[7] = 3;
  p[8] = 0;
  var uv = new Float32Array(6);
  uv[0] = 0;
  uv[1] = 0;
  uv[2] = 2;
  uv[3] = 0;
  uv[4] = 0;
  uv[5] = 2;
  triangle.setAttribute('position', new THREE.BufferAttribute(p, 3));
  triangle.setAttribute('uv', new THREE.BufferAttribute(uv, 2));

  this.mesh = new THREE.Mesh(triangle, this.material);
  this.scene = new THREE.Scene();
  this.scene.add(this.mesh);
  this.camera = new THREE.OrthographicCamera(-1, 1, 1, -1, 0, 1);

  this.clear = false;
};

ShaderPass.prototype.render = function (renderer, writeBuffer) {
  renderer.setRenderTarget(writeBuffer);
  renderer.render(this.scene, this.camera);
  renderer.setRenderTarget(null);
};

var SimulationRenderer = function (renderer, simMat, initMat, size) {
  // PRIVATE VARS

  var _this = this;

  var _renderer = renderer;
  var _size = size;

  var _target1, _target2, _target3, _outTargetPtr;
  var _simPass, _initPass, _debugPass;

  var _registeredUniforms = [];

  var _currUpdateTarget;

  // PRIVATE FUNCTIONS

  var _checkSupport = function () {
    var gl = renderer.getContext();

    if (gl.getExtension('OES_texture_float') === null) {
      console.error('SimulationRenderer: OES_texture_float not supported.');
      return false;
    }

    if (gl.getParameter(gl.MAX_VERTEX_TEXTURE_IMAGE_UNITS) === 0) {
      console.error(
        'SimulationRenderer: Vertex shader textures not supported.'
      );
      return false;
    }

    return true;
  };

  var _createTarget = function (s) {
    var target = new THREE.WebGLRenderTarget(s, s, {
      minFilter: THREE.NearestFilter,
      magFilter: THREE.NearestFilter,
      format: THREE.RGBAFormat,
      type: THREE.FloatType,
      depthBuffer: false,
      stencilBuffer: false
    });
    target.texture.generateMipmaps = false;
    return target;
  };

  var _updateRegisteredUniforms = function () {
    for (var i = 0; i < _registeredUniforms.length; i++) {
      _registeredUniforms[i].value = _outTargetPtr;
    }
  };

  // PUBLIC FUNCTIONS

  this.update = function (dt, t) {
    _simPass.material.uniforms.uDeltaT.value = dt;
    _simPass.material.uniforms.uTime.value = t;

    if (_currUpdateTarget === 1) {
      _simPass.material.uniforms.tPrev.value = _target2;
      _simPass.material.uniforms.tCurr.value = _target3;
      _simPass.render(_renderer, _target1);
      _outTargetPtr = _target1;
    } else if (_currUpdateTarget === 2) {
      _simPass.material.uniforms.tPrev.value = _target3;
      _simPass.material.uniforms.tCurr.value = _target1;
      _simPass.render(_renderer, _target2);
      _outTargetPtr = _target2;
    } else if (_currUpdateTarget === 3) {
      _simPass.material.uniforms.tPrev.value = _target1;
      _simPass.material.uniforms.tCurr.value = _target2;
      _simPass.render(_renderer, _target3);
      _outTargetPtr = _target3;
    } else {
      console.error("SimulationRenderer: something's wrong!");
    }

    // _debugPass.render(_renderer);

    // update uniforms
    _updateRegisteredUniforms();

    // increment target
    _currUpdateTarget++;
    if (_currUpdateTarget > 3) _currUpdateTarget = 1;
  };

  this.registerUniform = function (uniform) {
    _registeredUniforms.push(uniform);
    uniform.value = _outTargetPtr;
  };

  this.reset = function () {
    _initPass.render(_renderer, _target1);
    _initPass.render(_renderer, _target2);
    _initPass.render(_renderer, _target3);
  };

  // INITIALIZATION

  _checkSupport();

  // init targets

  _target1 = _createTarget(_size);
  _target2 = _createTarget(_size);
  _target3 = _createTarget(_size);

  _target1.name = 'SimulationRenderer._target1'; // debug tags
  _target2.name = 'SimulationRenderer._target2';
  _target3.name = 'SimulationRenderer._target3';

  _currUpdateTarget = 1;
  _outTargetPtr = _target1;

  // init shader pass

  _simPass = new ShaderPass(simMat);
  _initPass = new ShaderPass(initMat);
  // _debugPass = new ShaderPass(SimDebugShader);
  // _debugPass.material.uniforms.tTarget1.value = _target1;
  // _debugPass.material.uniforms.tTarget2.value = _target2;
  // _debugPass.material.uniforms.tTarget3.value = _target3;

  this.reset(); // reset targets
};

SimulationRenderer.prototype.constructor = SimulationRenderer;

/**
 * params = {
 *   simMat: THREE.Material,
 *   initMat: THREE.Material,
 *   drawMat: THREE.Material
 * }
 */
var ParticleSimulation = function (renderer, size, params) {
  var _this = this;
  var _size = size;
  var _sim, _simMat, _initMat, _drawMat, _particles;

  params = params || {};

  var _createParticleGeometry = function (size) {
    var ATTR_WIDTH = 3;
    var geo = new THREE.BufferGeometry();
    var pos = new Float32Array(size * size * ATTR_WIDTH);
    for (var x = 0; x < size; x++)
      for (var y = 0; y < size; y++) {
        var idx = x + y * size;
        pos[ATTR_WIDTH * idx] = (x + 0.5) / size; // +0.5 to be at center of texel
        pos[ATTR_WIDTH * idx + 1] = (y + 0.5) / size;
        pos[ATTR_WIDTH * idx + 2] = idx / (size * size); // extra: normalized id
      }
    geo.setAttribute('position', new THREE.BufferAttribute(pos, ATTR_WIDTH));
    return geo;
  };

  var _init = function () {
    _simMat = params.simMat || createShaderMaterial(SimShader);

    _initMat = params.initMat || createShaderMaterial(SimInitShader);

    _sim = new SimulationRenderer(renderer, _simMat, _initMat, _size);

    _drawMat = params.drawMat || createShaderMaterial(ParticleShader);
    _drawMat.blending = THREE.AdditiveBlending;
    _drawMat.transparent = true;
    _drawMat.depthTest = false;
    _drawMat.depthWrite = false;
    _sim.registerUniform(_drawMat.uniforms.tPos);

    var geo = _createParticleGeometry(_size);
    _particles = new THREE.Points(geo, _drawMat);
    _particles.frustumCulled = false;
  };

  this.getParticleObject = function () {
    return _particles;
  };

  _init();

  this.update = _sim.update;
};

var UVMapper = function (renderer) {
  var _renderer = renderer;

  var _camera = new THREE.OrthographicCamera(-1, 1, 1, -1, 0, 1); // not really used
  var _scene = new THREE.Scene();

  var _mat = createShaderMaterial(UVMapShader);
  _mat.side = THREE.DoubleSide;
  _mat.blending = THREE.NoBlending;
  _mat.depthTest = false;
  _mat.depthWrite = false;
  _mat.morphTargets = true;
  _scene.overrideMaterial = _mat;

  this.render = function (mesh, target) {
    // might need to make geo, so don't steal from its original parent
    _scene.add(mesh);
    _renderer.render(_scene, _camera, target, true);
    _scene.remove(mesh);
  };

  this.createTarget = function (size) {
    var target = new THREE.WebGLRenderTarget(size, size, {
      minFilter: THREE.LinearFilter,
      magFilter: THREE.LinearFilter,
      format: THREE.RGBAFormat,
      type: THREE.FloatType,
      depthBuffer: false,
      stencilBuffer: false
    });
    target.texture.generateMipmaps = false;

    return target;
  };

  this.createMap = function (mesh, size) {
    var target = this.createTarget(size);
    this.render(mesh, target);
    return target;
  };
};

var UVMapAnimator = function (renderer, size) {
  var _this = this;
  var _mesh;
  var _mapper = new UVMapper(renderer);
  var _speed = 1.0;

  this.target = _mapper.createTarget(size);

  this.update = function (dt, t) {
    if (!_mesh) return;

    _mesh.updateAnimation(dt * 1000);
    _mapper.render(_mesh, _this.target);
  };

  this.setMesh = function (mesh) {
    _mesh = mesh;
  };
};

var LeapManager = function (renderer, camera, transform) {
  var _scene, _root, _controller;

  var MAX_HANDS = 4; // apparently leap can have more than 2 hands
  // more than MAX_HANDS will draw, from plugin
  // but will not be taken into account

  this.renderer = renderer;
  this.camera = camera;
  this.matrix = transform;

  // PUBLIC

  this.followCamera = true;

  // read-only
  this.frame = undefined;
  this.palmPositions = [];
  for (var i = 0; i < MAX_HANDS; i++)
    this.palmPositions.push(new THREE.Vector3());
  this.activeHandCount = 0;
  // this.jointPositions = []; for (var i=0; i<50; i++) this.jointPositions.push(new THREE.Vector3());
  // this.activeJointCount = 0;

  // FUNCTIONS

  this.update = function () {
    // update leapRoot transform
    if (this.followCamera)
      _root.matrix.multiplyMatrices(this.camera.matrix, this.matrix);
    else _root.matrix.copy(this.matrix);
    _root.matrixWorldNeedsUpdate = true;

    // grab frame data
    this.frame = _controller.frame();

    // extract palm positions
    this.activeHandCount = Math.min(this.frame.hands.length, MAX_HANDS);
    for (var h = 0; h < this.activeHandCount; h++) {
      var pos = this.frame.hands[h].palmPosition;
      this.palmPositions[h].set(pos[0], pos[1], pos[2]);
      this.palmPositions[h].applyMatrix4(_root.matrix);
    }

    // this.activeJointCount = this.activeHandCount * 25;

    // extract joint positions
    // var jointIdx = 0;
    // for (var h=0; h<this.frame.hands.length; h++) {
    //     var hand = this.frame.hands[h];
    //     for (var f=0; f<hand.fingers.length; f++) {
    //         var finger = hand.fingers[f];
    //         for (var j=0; j<finger.positions.length; j++) {
    //             var rawpos = finger.positions[j];
    //             this.jointPositions[jointIdx].set(rawpos[0],rawpos[1],rawpos[2]);
    //             this.jointPositions[jointIdx].applyMatrix4(_root.matrix);
    //             jointIdx++;
    //         }
    //     }
    // }
  };

  this.render = function (renderTarget, forceClear) {
    this.renderer.render(_scene, this.camera, renderTarget, forceClear);
  };

  // INIT

  _scene = new THREE.Scene();
  _scene.overrideMaterial = new THREE.MeshBasicMaterial({
    color: 0xffffff,
    opacity: 0.25
  });

  _root = new THREE.Object3D();
  _root.matrixAutoUpdate = false;
  _scene.add(_root);

  _controller = new Leap.Controller();
  _controller
    .use('boneHand', {
      render: function () {},
      scene: _root,
      arm: false
    })
    .connect();
};

export function App() {
  var _engine;
  var _currPreset = 'sphere'; // initial preset
  var _currSimMode;
  var _uvAnim;

  // DEFINES

  function rgbaColor(value, n) {
    if (value[0] === '#') value = Utils.hexToRgb(value);
    ParticleShader.uniforms['uColor' + n].value.x = value[0] / 255.0;
    ParticleShader.uniforms['uColor' + n].value.y = value[1] / 255.0;
    ParticleShader.uniforms['uColor' + n].value.z = value[2] / 255.0;
  }
  rgbaColor('#666fff', 1);
  rgbaColor('#1985ff', 2);

  var _params = {
    size: 512,
    simMat: createShaderMaterial(SimShader),
    drawMat: createShaderMaterial(ParticleShader),
    update: undefined // defined later in the file
  };

  var _simModes = [
    'SIM_PLANE',
    'SIM_CUBE',
    'SIM_DISC',
    'SIM_SPHERE',
    'SIM_BALL',
    'SIM_ROSE_GALAXY',
    'SIM_GALAXY',
    'SIM_NOISE',
    'SIM_TEXTURE'
  ];

  var _presets = {
    sphere: { 'user gravity': 4, 'shape gravity': 3, _shape: 'SIM_SPHERE' }
  };

  // FUNCTIONS

  var _setSimMode = function (name) {
    if (name === _currSimMode) return;
    _currSimMode = name; // cache mode, prevent shader recompile

    _simModes.forEach(function (s) {
      delete _params.simMat.defines[s];
    });
    if (name) _params.simMat.defines[name] = '';
    _params.simMat.needsUpdate = true;
  };

  var _setPreset = function (name) {
    var preset = _presets[name] || _presets.none;
    _currPreset = name;

    // set shape
    if (preset._shape.length >= 0) {
      _setSimMode(preset._shape);
      _uvAnim.setMesh(); // set no mesh
    } else {
      _setSimMode('SIM_TEXTURE');
      _uvAnim.setMesh(preset._shape.mesh);
    }
  };

  // INIT

  var _init = function () {
    _engine = new ParticleEngine(_params);

    _uvAnim = new UVMapAnimator(_engine.renderer.getRenderer(), _params.size);
    _params.simMat.uniforms.tTarget = { type: 't', value: _uvAnim.target };
  };

  // RUN PROGRAM

  _init();
  _setPreset(_currPreset);
  // _engine.start();
  return _engine;
}

var SimShader = {
  defines: {
    K_VEL_DECAY: '0.99'
  },

  uniforms: {
    tPrev: { type: 't', value: null },
    tCurr: { type: 't', value: null },
    uDeltaT: { type: 'f', value: 0.0 },
    uTime: { type: 'f', value: 0.0 },
    uInputPos: {
      type: 'v3v',
      value: [
        new THREE.Vector3(),
        new THREE.Vector3(),
        new THREE.Vector3(),
        new THREE.Vector3()
      ]
    },
    uInputPosAccel: { type: 'v4', value: new THREE.Vector4(0, 0, 0, 0) },
    uInputAccel: { type: 'f', value: 3.0 },
    uShapeAccel: { type: 'f', value: 1.0 }
  },

  vertexShader: Utils.loadTextFile(BASE_URL + 'assets/shaders/Basic.vs.glsl'),

  fragmentShader: Utils.loadTextFileInject(
    BASE_URL + 'assets/shaders/SimShader.fs.glsl'
  )
};
if (Utils.isMobile) SimShader.defines.MULTIPLE_INPUT = '';

var BasicSimShader = {
  defines: {
    K_VEL_DECAY: '0.99',
    K_INPUT_ACCEL: '2.0',
    K_TARGET_ACCEL: '0.2'
  },

  uniforms: {
    tPrev: { type: 't', value: null },
    tCurr: { type: 't', value: null },
    uDeltaT: { type: 'f', value: 0.0 },
    uTime: { type: 'f', value: 0.0 },
    uInputPos: {
      type: 'v3v',
      value: [
        new THREE.Vector3(),
        new THREE.Vector3(),
        new THREE.Vector3(),
        new THREE.Vector3()
      ]
    },
    uInputPosAccel: { type: 'v4', value: new THREE.Vector4(0, 0, 0, 0) }
  },

  vertexShader: Utils.loadTextFile(BASE_URL + 'assets/shaders/Basic.vs.glsl'),

  fragmentShader: Utils.loadTextFile(
    BASE_URL + 'assets/shaders/BasicSimShader.fs.glsl'
  )
};
if (Utils.isMobile) BasicSimShader.defines.MULTIPLE_INPUT = '';

var SimInitShader = {
  uniforms: {
    tDiffuse: { type: 't', value: null },
    uColor: { type: 'f', value: new THREE.Vector4(1.0, 1.0, 1.0, 1.0) }
  },

  vertexShader: Utils.loadTextFile(BASE_URL + 'assets/shaders/Basic.vs.glsl'),

  fragmentShader: Utils.loadTextFileInject(
    BASE_URL + 'assets/shaders/SimInitShader.fs.glsl'
  )
};

var SimInitShader = {
  uniforms: {
    tDiffuse: { type: 't', value: null },
    uColor: { type: 'f', value: new THREE.Vector4(1.0, 1.0, 1.0, 1.0) }
  },

  vertexShader: Utils.loadTextFile(BASE_URL + 'assets/shaders/Basic.vs.glsl'),

  fragmentShader: Utils.loadTextFileInject(
    BASE_URL + 'assets/shaders/SimInitShader.fs.glsl'
  )
};

var ParticleShader = {
  uniforms: {
    tPos: { type: 't', value: null },
    uTime: { type: 'f', value: 0.0 },
    uPointSize: { type: 'f', value: 1.2 },
    uAlpha: { type: 'f', value: 0.2 },
    uColor1: { type: 'v3', value: new THREE.Vector3(1.0, 0.6, 0.1) },
    uColor2: { type: 'v3', value: new THREE.Vector3(1.0, 0.4, 1.0) },
    uColorFreq: { type: 'f', value: 1.0 },
    uColorSpeed: { type: 'f', value: -4.32 }
  },

  vertexShader: Utils.loadTextFileInject(
    BASE_URL + 'assets/shaders/ParticleShader.vs.glsl'
  ),

  fragmentShader: Utils.loadTextFileInject(
    BASE_URL + 'assets/shaders/ParticleShader.fs.glsl'
  )
};

var BasicParticleShader = {
  defines: {
    POINT_SIZE: Utils.isMobile ? '5.0' : '1.0'
  },

  uniforms: {
    tPos: { type: 't', value: null },
    uColor: { type: 'v4', value: new THREE.Vector4(1.0, 0.6, 0.1, 0.2) }
  },

  vertexShader: Utils.loadTextFile(
    BASE_URL + 'assets/shaders/BasicParticleShader.vs.glsl'
  ),

  fragmentShader: Utils.loadTextFile(
    BASE_URL + 'assets/shaders/BasicParticleShader.fs.glsl'
  )
};

var SimDebugShader = {
  uniforms: {
    tTarget1: { type: 't', value: null },
    tTarget2: { type: 't', value: null },
    tTarget3: { type: 't', value: null }
  },

  vertexShader: Utils.loadTextFile(BASE_URL + 'assets/shaders/Basic.vs.glsl'),

  fragmentShader: [
    'varying vec2 vUv;',
    'uniform sampler2D tTarget1;',
    'uniform sampler2D tTarget2;',
    'uniform sampler2D tTarget3;',
    'void main() {',
    'if (vUv.x < 0.33) {',
    'gl_FragColor = texture2D(tTarget1, vec2(vUv.x/0.33, vUv.y));',
    '}',
    'else if (vUv.x < 0.66) {',
    'gl_FragColor = texture2D(tTarget2, vec2((vUv.x-0.33) * 3.0, vUv.y));',
    '}',
    'else {',
    'gl_FragColor = texture2D(tTarget3, vec2((vUv.x-0.66) * 3.0, vUv.y));',
    '}',
    '}'
  ].join('\n')
};

var UVMapShader = {
  uniforms: {},

  vertexShader: Utils.loadTextFileInject(
    BASE_URL + 'assets/shaders/UVMapShader.vs.glsl'
  ),

  fragmentShader: Utils.loadTextFileInject(
    BASE_URL + 'assets/shaders/UVMapShader.fs.glsl'
  )
};
