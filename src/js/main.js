/*
 * @Description: main.js
 * @Author: yao yu qing
 * @Date: 2021-03-17 11:40:32
 * @LastEditTime: 2021-04-28 19:02:12
 * @LastEditors: yyq
 * @FilePath: \Block-la\src\js\main.js
 */

// vender.js
import Swiper from 'swiper';
import 'jquery-mousewheel';
import anime from 'animejs';
import { WOW } from 'wowjs';
import { App } from './particle';
import RenderWave from './wave';
import throttle from 'lodash/throttle';

$(function () {
  if ($('.bodyindex')[0]) {
    let indexPage = new IndexPage();
    indexPage.init();
  } else if ($('.bodyproductpost')[0]) {
    let projectPostPage = new ProjectPostPage();
    projectPostPage.init();
  }
});

// 首页
class IndexPage {
  constructor() {
    // 粒子球体
    this._engine = App();
  }
  init() {
    this.indexSlider();
    this.menuControl();
    this.product();
    initStreamer(
      $('.contact .contact_form .form_wrapper .btn'),
      $('.contact .contact_form .form_wrapper .streamer')
    );

    let $topSwiper = $('.top-slider .swiper-container')[0].swiper;
    let _this = this;
    loading(
      function () {
        $topSwiper.autoplay.stop();
      },
      function () {
        initWow();
        _this.singleWheel($('.indexPage .module')).init($('.bodyindex'));
        setTimeout(() => {
          let _hash = +window.location.hash.substr(1);
          wowHidden(_hash);
          if (_hash === 0) {
            $topSwiper.autoplay.start();
          }
        }, 500);
      }
    );
  }
  // menu
  menuControl() {
    let _this = this;
    $('.bodyindex .menu_btn').on('click', function () {
      $(this).addClass('menu_on');
      $('.bodyindex').addClass('open_control');
      _this._engine.stop();
    });

    $('.lock-screen').on('click', function () {
      $('.bodyindex .menu_btn').removeClass('menu_on');
      $('.bodyindex').removeClass('open_control');
      _this._engine.start();
    });

    $('.module-control .moduleCItem').on('click', function () {
      $('.lock-screen').trigger('click');
    });
  }
  // slider
  indexSlider() {
    let $slider = $('.swiper-container');
    let _this = this;
    $slider.each(function () {
      let slide = $(this).data('slide');
      let autoplay = $(this).data('autoplay');
      let loop = $(this).data('loop');
      let nav = $(this).data('nav');
      let pagin = $(this).data('pagin');
      let moduleId = $(this).closest('.module').attr('id');

      _this.initSlider($(this), {
        slidesPerView: slide,
        slidesPerGroup: slide,
        loop: loop,
        navigation: nav
          ? {
              nextEl: '#' + moduleId + ' .swiper-button-next',
              prevEl: '#' + moduleId + ' .swiper-button-prev'
            }
          : false,
        pagination: pagin
          ? {
              el: '#' + moduleId + ' .swiper-pagination',
              type: 'fraction'
            }
          : false,
        autoplay: autoplay
          ? {
              disableOnInteraction: false,
              delay: 3000
            }
          : false
      });
    });
  }
  product() {
    let $itemBlock = $('.product .item_block');
    let $itemInfo = $('.product .item_info li');

    $('.more', $itemBlock).hover(
      function () {
        let index = $(this).parent().index();
        anime({
          targets: $itemInfo.eq(index)[0],
          opacity: 1,
          translateY: ['-10%', 0],
          endDelay: 0,
          easing: 'cubicBezier(0.22, 1, 0.36, 1)'
        });
      },
      function () {
        let index = $(this).parent().index();
        anime({
          targets: $itemInfo.eq(index)[0],
          opacity: 0,
          translateY: [0, '-10%'],
          endDelay: 0,
          easing: 'cubicBezier(0.22, 1, 0.36, 1)'
        });
      }
    );
    infoPos();
    $(window).on('resize', infoPos);
    function infoPos() {
      $itemInfo.each(function (i, el) {
        $(el).removeAttr('style');
        let posLeft = Math.floor($itemBlock.eq(i).offset().left);
        let blockWidth = $itemBlock.eq(i).width();
        let screenWidth = $(document).width();
        let screenCenter = screenWidth / 2;

        if (posLeft + blockWidth > screenCenter) {
          $(el).css({
            right: screenWidth - posLeft
          });
        } else {
          $(el).css({
            left: posLeft + blockWidth
          });
        }
      });
    }

    // swiper transitionEnd
    let $swiperEl = $('.product .swiper-container');
    $swiperEl[0].swiper.on('slideChangeTransitionEnd', function () {
      infoPos();
    });
  }
  // 幻灯初始化
  initSlider($slider, options) {
    if (!$slider[0]) return;
    let settings = {
      roundLengths: true,
      watchOverflow: true,
      autoplay: false,
      loop: false,
      speed: 1000,
      direction: 'horizontal',
      init: false
    };

    let slickOpt = $.extend(settings, options);
    let myswiper = new Swiper($slider, slickOpt);

    myswiper.init();
    return myswiper;
  }
  // 单屏滚动
  singleWheel(modules) {
    let _this = this;
    let $pages = modules;
    let isAnimating = false;
    let endCurrPage = false;
    let endNextPage = false;
    let animEndEventName = 'webkitAnimationEnd';
    let total = $pages.length;

    let $currPage, $nextPage;
    let currentClass = 'pt-page-current';
    let outClassDown = 'pt-page-moveToTopEasing';
    let inClassDown = 'pt-page-moveFromBottom pt-page-ontop';
    let outClassUp = 'pt-page-moveToBottomEasing';
    let inClassUp = 'pt-page-moveFromTop pt-page-ontop';

    $pages.eq(0).addClass(currentClass);

    let oper = {
      currentDot: function (n) {
        $('.moduleControl .moduleCItem').each(function () {
          $(this).removeClass('active');
        });

        $('.moduleControl .moduleCItem')
          .eq(n)

          .addClass('active');

        window.location.hash = `${n}`;
      },

      wheel: function (ev) {
        let pageIndex = window.location.hash.slice(1) || 0;
        if (ev.deltaY > 0) {
          if (isAnimating) return false;
          window.location.hash = Math.max(0, +pageIndex - 1);
        } else {
          if (isAnimating) return false;
          window.location.hash = Math.min(total - 1, +pageIndex + 1);
        }
      },

      onEndAnimation: function ($outpage, $inpage) {
        endCurrPage = false;
        endNextPage = false;
        oper.resetPage($outpage, $inpage);
        isAnimating = false;
        wowHidden($inpage.index());
        this.complete($inpage.index());
      },
      resetPage: function ($outpage, $inpage) {
        $outpage.removeClass(
          'pt-page-moveToTopEasing pt-page-moveToBottomEasing pt-page-current'
        );

        $inpage.removeClass(
          'pt-page-moveFromTop pt-page-moveFromBottom pt-page-delay200 pt-page-ontop'
        );
      },
      complete: function (index) {
        var $topSwiper = $('.top-slider .swiper-container')[0].swiper;
        $topSwiper.autoplay.stop();
        _this._engine.stop();
        $('.bodyindex .down_dot').removeClass('hide');
        switch (index) {
          case 0:
            $topSwiper.autoplay.start();
            break;
          case 1:
            _this._engine.start();
            break;
          case 3:
            $('.case .plane').addClass('anime');
            break;
          case 5:
            $('.bodyindex .down_dot').addClass('hide');
            break;
          default:
            return false;
        }
      }
    };

    $('.moduleControl .moduleCItem').click(function () {
      if (isAnimating) return false;

      $('.moduleControl .moduleCItem').each(function () {
        $(this).removeClass('active');
      });

      $(this).addClass('active');
    });

    let nowIndex = 0;

    pageGo(
      window.location.hash.slice(1) > total - 1
        ? total - 1
        : window.location.hash.slice(1)
    );

    function pageGo(target) {
      if (!$('.bodyindex').length || $('.bodyindex').hasClass('open_control'))
        return false;

      if (typeof +target !== 'number' || !$pages[target]) return false;

      if (nowIndex === +target) return false;

      let dir = target > nowIndex ? 'next' : 'prev';

      $currPage = $pages.eq(nowIndex);

      $('.module-control .moduleCItem')
        .eq(target)
        .addClass('active')
        .siblings()
        .removeClass('active');

      isAnimating = true;
      switch (dir) {
        case 'prev':
          $nextPage = $pages.eq(target).addClass(currentClass);

          oper.currentDot(target);

          $currPage.addClass(outClassUp).on(animEndEventName, function () {
            endCurrPage = true;

            if (endNextPage) {
              oper.onEndAnimation($currPage, $nextPage);
            }
          });

          $nextPage.addClass(inClassUp).on(animEndEventName, function () {
            $nextPage.off(animEndEventName);

            endNextPage = true;

            if (endCurrPage) {
              oper.onEndAnimation($currPage, $nextPage);
            }
          });

          break;

        case 'next':
          $nextPage = $pages.eq(target).addClass(currentClass);

          oper.currentDot(target);

          $currPage

            .addClass(outClassDown)

            .on(animEndEventName, function () {
              $currPage.off(animEndEventName);

              endCurrPage = true;

              if (endNextPage) {
                oper.onEndAnimation($currPage, $nextPage);
              }
            });

          $nextPage

            .addClass(inClassDown)

            .on(animEndEventName, function () {
              $nextPage.off(animEndEventName);

              endNextPage = true;

              if (endCurrPage) {
                oper.onEndAnimation($currPage, $nextPage);
              }
            });

          break;

        default:
          break;
      }

      nowIndex = target;
    }

    window.onhashchange = function () {
      let targetIndex = window.location.hash.slice(1);

      if (targetIndex) {
        pageGo(+targetIndex);
      }
    };

    return {
      init: function ($dom) {
        $dom.on(
          'mousewheel',
          throttle(oper.wheel, 2000, {
            leading: true,
            trailing: false
          })
        );
      }
    };
  }
}

// 产品列表页
class ProjectPostPage {
  init() {
    loading(initWow);
    RenderWave($('.ribbon'), document.querySelectorAll('.ribbon'));
    this.header();
    this.movebar();
    this.feature();
    this.postControl();
    initStreamer(
      $('.contact .contact_form .form_wrapper .btn'),
      $('.contact .contact_form .form_wrapper .streamer')
    );
  }
  header() {
    if ($(document).scrollTop() > 0) {
      $('#header').addClass('mini');
    }
    $(document).scroll(function () {
      if ($(document).scrollTop() > 0) {
        $('#header').addClass('mini');
      } else {
        $('#header').removeClass('mini');
      }
    });
  }
  movebar() {
    let $itemNav = $('.post-content .item-nav');
    let $movebar = $('.post-content .move-bar');
    let isScroll = true;
    $itemNav.hover(
      function () {
        let currentPos = $(this).position().left;
        anime({
          targets: $movebar[0],
          left: currentPos,
          endDelay: 0,
          easing: 'cubicBezier(0.22, 1, 0.36, 1)',
          complete: function () {
            isScroll = false;
          }
        });
      },
      function () {
        let $activeNav = $('.post-content .item-nav.active');
        let currentPos = $activeNav.position().left;
        anime({
          targets: $movebar[0],
          left: currentPos,
          endDelay: 0,
          easing: 'cubicBezier(0.22, 1, 0.36, 1)',
          complete: function () {
            isScroll = true;
          }
        });
      }
    );
    $itemNav.on('click', function () {
      isScroll = false;
      let _host = $('a', this).attr('href');
      let _targetPos = $(_host).offset();
      $(this).addClass('active').siblings().removeClass('active');

      $('html,body').animate(
        {
          scrollTop: _targetPos.top - 177
        },
        {
          duration: 600,
          easing: 'linear',
          complete: function () {
            isScroll = true;
          }
        }
      );
      return false;
    });

    pin(
      {
        containerSelector: '.bodyproductpost .post-content',
        activeClass: 'fixed',
        padding: {
          top: 81,
          bottom: 0
        },
        distance: {
          bottom: 200
        }
      },
      $('.bodyproductpost .post-nav')
    );

    $(document).on('scroll', function () {
      if (!isScroll) return false;
      let documentScrollTop = $(document).scrollTop();
      let bannerH = $('.banner').outerHeight();
      $itemNav.each(function (i, el) {
        let itemHref = $('a', el).attr('href');
        let currentH = $(itemHref).offset().top - bannerH;
        if (currentH < documentScrollTop) {
          $(el).addClass('active').siblings().removeClass('active');
          let currentPos = $(el).position().left;
          anime({
            targets: $movebar[0],
            left: currentPos,
            endDelay: 0,
            easing: 'cubicBezier(0.22, 1, 0.36, 1)'
          });
        }
      });
    });
  }
  feature() {
    let $block = $('.feature .item_block');
    $block.on('click', function () {
      $(this).addClass('active').siblings().removeClass('active');
    });
  }
  postControl() {
    $('.postlist.slider').each(function (i, el) {
      let elId = $(el).attr('id');
      let clicked = true;
      let swiper = new Swiper($('.swiper-container', el), {
        slidesPerView: 1,
        slidesPerGroup: 1,
        roundLengths: true,
        watchOverflow: true,
        autoplay: false,
        loop: false,
        speed: 1000,
        direction: 'horizontal',
        allowTouchMove: false,
        preventInteractionOnTransition: true,
        init: false,
        effect: i === 0 ? 'fade' : false,
        fadeEffect:
          i === 0
            ? {
                crossFade: true
              }
            : false,
        pagination: {
          el: '#' + elId + ' .swiper-pagination',
          type: 'progressbar',
          progressbarOpposite: true
        },
        on: {
          transitionStart: function () {
            clicked = false;
            $('.item-control', el)
              .eq(this.realIndex)
              .addClass('active')
              .siblings()
              .removeClass('active');
          },
          transitionEnd: function () {
            clicked = true;
          }
        }
      });
      swiper.init();

      // swiper control
      $('.item-control', el).on('click', function () {
        if (!clicked) return false;
        let _index = $(this).index();
        swiper.slideTo(_index);
        $(this).addClass('active').siblings().removeClass('active');
      });
    });
  }
}

// loading
function loading() {
  let begin = arguments[1] ? arguments[0] : null;
  let complete = arguments[1] ? arguments[1] : arguments[0];
  let tl = anime.timeline({
    targets: '.inner-mask',
    endDelay: 0,
    easing: 'cubicBezier(0.22, 1, 0.36, 1)'
  });

  tl.add({
    translateY: '100%',
    duration: 0,
    delay: 800
  })
    .add({
      duration: 1200,
      translateY: 0
    })
    .add({
      duration: 1200,
      translateY: '-100%',
      begin: begin,
      complete: function () {
        $('.body-mask').fadeOut(400);
        complete && complete();
      }
    });
}

// 流光移动
function initStreamer($currentEl, $moveEl) {
  const handleMouseEnter = e => {
    let _left = $currentEl.position().left;
    anime({
      targets: $moveEl[0],
      left: _left + 20,
      duration: 1500
    });
  };
  const handleMouseLeave = () => {
    anime({
      targets: $moveEl[0],
      left: 0,
      duration: 1500
    });
  };
  $currentEl.on('mouseenter', handleMouseEnter);
  $currentEl.on('mouseleave', handleMouseLeave);
}

// wowinit
function initWow() {
  window.wow = new WOW();
  window.wow.init();
}

// wow repeat
function wowHidden(index) {
  $('.wow').each(function () {
    if ($(this).closest($('.module')).index() === index) {
      var animationName = $(this).data('animation-name') || 'fadeInUp';
      $(this)
        .css({
          visibility: 'visible',
          'animation-name': animationName
        })
        .addClass('animated');
    } else {
      $(this)
        .css({
          visibility: 'hidden',
          'animation-name': 'none'
        })
        .removeClass('animated');
    }
  });
}

// jq.pin
function pin(options, $dom) {
  var scrollY = 0;
  var elements = [];
  var disabled = false;
  var $window = $(window);

  options = $.extend(
    {
      distance: {
        bottom: 0
      }
    },
    options
  );

  var recalculateLimits = function () {
    for (var i = 0, len = elements.length; i < len; i++) {
      var $this = elements[i];

      if (options.minWidth && $window.width() <= options.minWidth) {
        if ($this.parent().is('.pin-wrapper')) {
          $this.unwrap();
        }
        $this.css({
          width: '',
          left: '',
          top: '',
          position: ''
        });
        if (options.activeClass) {
          $this.removeClass(options.activeClass);
        }
        disabled = true;
        continue;
      } else {
        disabled = false;
      }

      var $container = options.containerSelector
        ? $this.closest(options.containerSelector)
        : $(document.body);
      var offset = $this.offset();
      var containerOffset = $container.offset();
      var parentOffset = $this.offsetParent().offset();

      if (!$this.parent().is('.pin-wrapper')) {
        $this.wrap('<div class="pin-wrapper"></div>');
      }

      var pad = $.extend(
        {
          top: 0,
          bottom: 0
        },
        options.padding || {}
      );

      $this.data('pin', {
        pad: pad,
        from:
          (options.containerSelector ? containerOffset.top : offset.top) -
          pad.top,
        to:
          containerOffset.top +
          $container.height() -
          $this.outerHeight() -
          pad.bottom,
        end: containerOffset.top + $container.height(),
        parentTop: parentOffset.top
      });

      $this.removeAttr('style').css({ width: $this.outerWidth() });
      $this.parent().removeAttr('style').css('height', $this.outerHeight());
    }
  };

  var onScroll = function () {
    if (disabled) {
      return;
    }

    scrollY = $window.scrollTop();

    var elmts = [];
    for (var i = 0, len = elements.length; i < len; i++) {
      var $this = $(elements[i]);
      var data = $this.data('pin');

      if (!data) {
        // Removed element
        continue;
      }

      elmts.push($this);

      var from = data.from;
      var to = data.to - options.distance.bottom;

      if (from + $this.outerHeight() > data.end) {
        $this.css('position', '');
        continue;
      }

      if (from < scrollY && to > scrollY) {
        !($this.css('position') === 'fixed') &&
          $this
            .css({
              left: '',
              top: data.pad.top
            })
            .css('position', 'fixed');
        if (options.activeClass) {
          $this.addClass(options.activeClass);
        }
      } else if (scrollY >= to) {
        $this
          .css({
            left: '',
            top: to - data.parentTop
          })
          .css('position', 'absolute');
        if (options.activeClass) {
          $this.addClass(options.activeClass);
        }
      } else {
        $this.css({ position: '', top: '', left: '' });
        if (options.activeClass) {
          $this.removeClass(options.activeClass);
        }
      }
    }
    elements = elmts;
  };

  var update = function () {
    recalculateLimits();
    onScroll();
  };

  $dom.each(function (index, item) {
    var $item = $(item);
    var data = $item.data('pin') || {};

    if (data && data.update) {
      return;
    }
    elements.push($item);
    $('img', $item).one('load', recalculateLimits);
    data.update = update;
    $item.data('pin', data);
  });

  $window.on('scroll', onScroll);
  $window.on('resize', function () {
    recalculateLimits();
  });
  recalculateLimits();

  $window.on('load', update);

  return update;
}
