/*
 * @Description: base data
 * @Author: yao yu qing
 * @Date: 2021-04-06 13:18:49
 * @LastEditTime: 2021-04-29 16:24:39
 * @LastEditors: yyq
 * @FilePath: \Block-la\src\data\base.js
 */
const BASE_URL = process.env.NODE_ENV === 'development' ? '/' : './';

const BASE_DATA = {
  BASE_URL,
  headTag: {
    title: 'Block-la'
  },
  base: {
    header: {
      logo: BASE_URL + 'assets/images/logo.png',
      nav: [
        {
          name: 'Block.la产品',
          url: 'javascript:;',
          active: true,
          subnav: [
            [
              {
                name: '现货交易系统',
                url: './product_post.html',
                active: true
              },
              {
                name: '合约交易系统',
                url: 'javascript:;',
                active: false
              },
              {
                name: '去中心化钱包系统',
                url: 'javascript:;',
                active: false
              },
              {
                name: '杠杆交易系统',
                url: 'javascript:;',
                active: false
              }
            ],
            [
              {
                name: '资管和金融中心',
                url: 'javascript:;',
                active: false
              },
              {
                name: '流动性提供系统',
                url: 'javascript:;',
                active: false
              },
              {
                name: 'etf交易系统',
                url: 'javascript:;',
                active: false
              }
            ]
          ]
        },
        {
          name: '解决方案',
          url: 'javascript:;',
          active: false
        },
        {
          name: '客户案例',
          url: 'javascript:;',
          active: false
        },
        {
          name: '获取报价',
          url: 'javascript:;',
          active: false
        },
        {
          name: 'Block.la动态',
          url: 'javascript:;',
          active: false
        },
        {
          name: '关于我们',
          url: 'javascript:;',
          active: false
        }
      ],
      lang: [
        '简体中文',
        'English',
        'Bahasa Indonesia',
        'Espanõl',
        'Français',
        '한국어'
      ]
    }
  }
};

module.exports = BASE_DATA;
