/*
 * @Description: gulpfile.js文件
 * @Author: yao yu qing
 * @Date: 2020-11-10 13:55:40
 * @LastEditTime: 2021-04-09 14:44:53
 * @LastEditors: yyq
 * @FilePath: \Block-la\gulpfile.js
 */
const baseConf = require('./build/webpack.base.config');
const devServerOptions = require('./config/dev-server-config');
const merge = require('webpack-merge');
// 让日志更加友好
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin');
// 查找开放端口或域接字的简单工具
const portfinder = require('portfinder');
const webpack = require('webpack');
// const webpackStream = require("webpack-stream");
const WebpackDevServer = require('webpack-dev-server');
const ora = require('ora');
const chalk = require('chalk');
// gulp
const { series, src } = require('gulp');
const fontSpider = require('gulp-font-spider');
const plumber = require('gulp-plumber');
const del = require('del');

let config, devConf;

function webpackDevConf() {
  config = require('./build/webpack.dev.config');
  devConf = merge(baseConf, config);
  portfinder.basePort = devServerOptions.port;
  portfinder
    .getPortPromise()
    .then(port => {
      // add port to devServer config
      devServerOptions.port = port;
      // Add FriendlyErrorsPlugin
      devConf.plugins.push(
        new FriendlyErrorsPlugin({
          compilationSuccessInfo: {
            messages: [
              `Your application is running here: http://${devServerOptions.host}:${port}`
            ]
          },
          clearConsole: true
        })
      );
      WebpackDevServer.addDevServerEntrypoints(devConf, devServerOptions);
      const compiler = webpack(devConf);
      const server = new WebpackDevServer(compiler, devServerOptions);
      server.listen(devServerOptions.port, devServerOptions.host);
    })
    .catch(err => {
      console.log(chalk.red(err));
    });
}

function webpackProductConf(done) {
  config = require('./build/webpack.prod.config');
  const spinner = ora('building for production...');
  spinner.start();
  const compiler = webpack(merge(baseConf, config));
  compiler.run((err, stats) => {
    spinner.stop();
    if (err) throw err;
    process.stdout.write(
      stats.toString({
        colors: true,
        modules: false,
        children: false,
        chunks: false,
        chunkModules: false
      }) + '\n\n'
    );

    if (stats.hasErrors()) {
      console.log(chalk.red('  Build failed with errors.\n'));
      process.exit(1);
    }

    console.log(chalk.cyan('  Build complete.\n'));
    console.log(
      chalk.yellow(
        '  Tip: built files are meant to be served over an HTTP server.\n' +
          "  Opening index.html over file:// won't work.\n"
      )
    );
    done();
  });
}

// font压缩
function fontspider() {
  return src('dist/*.html').pipe(plumber()).pipe(fontSpider());
}

// 清空font压缩备份文件
function cleanFontSpiderFile(done) {
  del('dist/assets/fonts/.font-spider', done());
}

exports.default = series(webpackDevConf);
exports.dev = series(webpackDevConf);
exports.prod = series(webpackProductConf, fontspider, cleanFontSpiderFile);
