module.exports = {
  rootDir: './dist',
  scssSrc: './src/scss/**/*.scss',
  jsSrc: './src/js/**/*.js',
  imgSrc: './src/img/**/*',
  outCss: './dist/assets/css/*.css',
  outJs: './dist/js',
  outImg: './dist/'
}
