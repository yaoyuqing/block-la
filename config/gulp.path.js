module.exports = {
    rootDir: "./dist",
    scssSrc: "./src/scss/**/*.scss",
    jsSrc: "./src/js/**/*.js",
    imgSrc: "./src/img/**/*",
    outCss: "./dist/css",
    outJs: "./dist/js",
    outImg: "./dist/",
};
